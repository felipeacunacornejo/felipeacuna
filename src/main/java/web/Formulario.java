package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


@WebServlet("/Formulario")
public class Formulario extends HttpServlet{

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println("Este es tu nombre: ");
        String nombre = request.getParameter("nombre");
        out.println(nombre);
        System.out.println(" ");
        out.println("Este es tu sección: ");
        String seccion = request.getParameter("seccion");
        out.println(seccion);

    }
    
}