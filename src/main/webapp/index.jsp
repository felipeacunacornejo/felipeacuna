<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
        <title>Actividad Taller 1 U1</title>
    </head>
    <body>
    <center>
        <form id="formData" action="/FelipeAcuna/Formulario" method="post" >
            <div class="row">
                    <div class="col-md-12">
                        <div class='card w-90'>
                            <div class='card-body'>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <div class="md-form md-outline">
                                                    <label class="upper" for="nombre">Nombre</label>
                                                    <input type="text" class="form-control required" placeholder="" id="nombre" name="nombre" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="md-form md-outline">
                                                    <label class="upper" for="seccion">Sección</label>
                                                    <input type="text" class="form-control required" placeholder="" id="seccion" name="seccion" required>
                                                </div>
                                            </div>
                                            <br/>
                                            <br/>
                                            <div class="col-md-6">
                                                <div class="md-form md-outline">
                                                    <input type="submit" class="btn btn-primary" value="Enviar">
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </center>        
    </body>
</html>
